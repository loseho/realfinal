class Calculator

  def self.candidate_all(a, b)
    c = (Candidate.where("created_at >= ? AND created_at <= ?",a,b)).count
    return "All Candidate = "+c.to_s
  end

  def self.candidate_pass(a, b)
    c = 0
    can = Candidate.all
    can.each do |f|
      can_pro = f.progressions.where("created_at >= ? AND created_at <= ?",a,b)
      can_pro.each do |g|
          if(g.status.upcase == "PASS")
            c+=1
          end
        end
      end
    return "Passed Candidate = "+c.to_s
  end

  def self.candidate_reject(a, b)
    c = 0
    can = Candidate.all
    can.each do |f|
      can_pro = f.progressions.where("created_at >= ? AND created_at <= ?",a,b)
      can_pro.each do |g|
        if(g.status.upcase == "REJECT")
          c+=1
        end
      end
    end
    return "Rejected Candidate = "+c.to_s
  end

end