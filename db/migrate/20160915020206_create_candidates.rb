class CreateCandidates < ActiveRecord::Migration[5.0]
  def change
    create_table :candidates do |t|
      t.string :name
      t.string :email
      t.string :gender
      t.string :phone
      t.string :address
      t.string :source
      t.string :source_detail
      t.string :position
      t.integer :test_general
      t.integer :test_techtical
      t.float :pre_salary
      t.float :expect_salary
      t.boolean :blacklist

      t.timestamps
    end
  end
end
