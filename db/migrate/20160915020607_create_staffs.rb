class CreateStaffs < ActiveRecord::Migration[5.0]
  def change
    create_table :staffs do |t|
      t.string :name
      t.string :email
      t.string :gender
      t.string :phone
      t.string :position
      t.string :password

      t.timestamps
    end
  end
end