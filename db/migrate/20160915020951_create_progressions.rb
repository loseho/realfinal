class CreateProgressions < ActiveRecord::Migration[5.0]
  def change

    create_table :progressions do |t|
      t.string :status
      t.belongs_to :candidate, index: true
      t.belongs_to :staff, index: true

      t.timestamps
    end
  end
end
