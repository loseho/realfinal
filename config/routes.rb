Rails.application.routes.draw do
  resources :staffs
  resources :candidates do
    resources :progressions
  end
  resources :calendars
  resources :statistics


  # get  'candidates/:candidate_id/newProgression' , to: 'candidates#newProgression' , as: 'newProgression'
  # post 'candidates/:candidate_id/newProgression' , to: 'candidates#createProgression'

  # post 'progression' , to: 'candidates#newProgression'

  root 'candidates#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
