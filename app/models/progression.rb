class Progression < ApplicationRecord
  belongs_to :candidate
  belongs_to :staff

  validates :status,  presence: true, length: { maximum: 255 }
  validates :staff_id, presence: true
  validates :status, presence: true, length: { maximum: 255 }
end
