class Calendar < ApplicationRecord
  belongs_to :candidate
  belongs_to :staff

  validates :event,  presence: true, length: { maximum: 255 }
  validates :event_date,  presence: true, length: { maximum: 255 }
  validates :period,  presence: true, length: { maximum: 255 }
  validates :candidate_id,  presence: true, length: { maximum: 255 }
  validates :staff_id,  presence: true, length: { maximum: 255 }

  validate :calendar_check

  def calendar_check
    Calendar.where(event_date: event_date).each do |i|
        if(i.period == period && i.event != event)
          errors.add(:event_date, "Schedule is already full")
        end
      end
    Calendar.where(event_date: event_date).each do |i|
      if(i.period == period && i.staff_id == staff_id)
          errors.add(:event_date, "Schedule is already full")
      end
    end
   end
end