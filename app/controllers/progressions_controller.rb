class ProgressionsController < ApplicationController
  before_action :set_progression, only: [ :edit ]

  def edit
    @candidate = Candidate.find(params[:candidate_id])
    @progression = @candidate.progressions.new
  end

  def new
    @candidate = Candidate.find(params[:candidate_id])
    @progression = @candidate.progressions.new
  end

  def create
    @candidate = Candidate.find(params[:candidate_id])
    @progression = @candidate.progressions.create(progression_params)
    redirect_to candidates_path
  end

  private
  def set_progression
    @progression = Progression.find(params[:id])
  end

  def progression_params
    params.require(:progression).permit(:status, :staff_id)
  end
end