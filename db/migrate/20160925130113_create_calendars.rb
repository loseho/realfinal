class CreateCalendars < ActiveRecord::Migration[5.0]
  def change
    create_table :calendars do |t|
      t.string :event
      t.string :period
      t.datetime :event_date
      t.belongs_to :candidate, index: true
      t.belongs_to :staff, index: true

      t.timestamps
    end
  end
end
